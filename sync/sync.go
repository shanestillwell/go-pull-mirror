package sync

import (
	// "context"
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/xanzy/go-gitlab"
	"golang.org/x/time/rate"
)

type Client struct {
	gitlab *gitlab.Client
	limiter *rate.Limiter
}

func NewClient(glToken string) (*Client, error) {
	c := Client{}
	git, err := gitlab.NewClient(glToken)
	if err != nil {
		log.Fatalf("Failed to create GitLab client %v", err)
	}
	c.gitlab = git

	// 10 requests per second will keep us well under GitLab's rate limit
	c.limiter = rate.NewLimiter(rate.Every(time.Second), 10)
	return &c, nil
}

func (client *Client) sendProjectUpdate (projectId int, projectOptions *gitlab.EditProjectOptions, ctx context.Context, wg *sync.WaitGroup) (project *gitlab.Project, err error) {
		defer wg.Done()
    err = client.limiter.Wait(ctx)
    if err != nil {
			fmt.Println("Failed to wait for rate limiter")
			return nil, err
    }
		project, _, err = client.gitlab.Projects.EditProject(projectId, projectOptions)
		// fmt.Printf("Result %+v\n", result)
		if err != nil {
			fmt.Printf("Failed to edit project %d: %v\n", projectId, err)
			return nil, err
		}
		// fmt.Printf("%d, %+v\n", result.StatusCode, project)
		return project, nil
}

func Sync(results map[int]string, adoToken string, client *Client) {
	ctx := context.Background()

	// Use a WatiGroup so the main program waits for our goroutines to finish
	var wg sync.WaitGroup
	wg.Add(len(results))

	// Iterate over the map to access all key-value pairs
	for projectId, value := range results {
		url := getAdoUrl(adoToken, value, "foo")

		// Edit project options
		p := &gitlab.EditProjectOptions{
			ImportURL:                        gitlab.Ptr(url),
			Mirror:                           gitlab.Ptr(true),
			MirrorTriggerBuilds:              gitlab.Ptr(true),
			MirrorOverwritesDivergedBranches: gitlab.Ptr(true),
		}
		// fmt.Printf("%d, %+v\n", projectId, p)
		go client.sendProjectUpdate(projectId, p, ctx, &wg)
	}
	wg.Wait()
}

func getAdoUrl(adoToken, adoBase, adoPath string) string {
	return "https://" + adoToken + "@" + adoBase + "/" + adoPath
}

