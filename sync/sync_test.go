package sync

import (
	"fmt"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
	"golang.org/x/time/rate"
)

// setup sets up a test HTTP server along with a gitlab.Client that is
// configured to talk to that test server.  Tests should register handlers on
// mux which provide mock responses for the API method being tested.
func setup(t *testing.T) (*http.ServeMux, *gitlab.Client) {
	// mux is the HTTP request multiplexer used with the test server.
	mux := http.NewServeMux()

	// server is a test HTTP server used to provide mock API responses.
	server := httptest.NewServer(mux)
	t.Cleanup(server.Close)

	// client is the Gitlab client being tested.
	client, err := gitlab.NewClient("",
		gitlab.WithBaseURL(server.URL),
		// Disable backoff to speed up tests that expect errors.
		gitlab.WithCustomBackoff(func(_, _ time.Duration, _ int, _ *http.Response) time.Duration {
			return 0
		}),
	)
	if err != nil {
		t.Fatalf("Failed to create client: %v", err)
	}

	return mux, client
}

func TestSync(t *testing.T) {
	mux, client := setup(t)
	a := assert.New(t)

	mux.HandleFunc("/api/v4/projects/123", func(w http.ResponseWriter, r *http.Request) {
    var p gitlab.Project
    err := json.NewDecoder(r.Body).Decode(&p)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		a.Equal(r.Method, "PUT", "should be equal")
		a.True(p.Mirror, "Mirror should be set to true")
		a.True(p.MirrorTriggerBuilds, true, "MirrorTriggerBuilds should be true")
		a.True(p.MirrorOverwritesDivergedBranches, true, "MirrorOverwritesDivergedBranches should be true")
		a.Contains(p.ImportURL, "FAKE_TOKEN", "ImportURL should contain token")
		// fmt.Printf("JSON %+v\n", p)
		fmt.Fprint(w, `{"id":1}`)
	})

	c := Client{}
	c.gitlab = client
	c.limiter = rate.NewLimiter(rate.Every(time.Millisecond), 10000)

	m := make(map[int]string)
	m[123] = "Quantum/one/_git/three"

	Sync(m, "FAKE_TOKEN", &c)
}

func TestGetAdoUrl(t *testing.T) {
	url := getAdoUrl("FAKE_TOKEN", "dev.azure.com", "Quantum/one/_git/three")
	a := assert.New(t)
	a.Equal("https://FAKE_TOKEN@dev.azure.com/Quantum/one/_git/three", url, "should be equal")
}
