package cmd

import (
	// "fmt"
	"log"

	"github.com/spf13/cobra"
	"pull-mirror/sync"
	"pull-mirror/parse"
)

var glToken string
var adoToken string
var filename string

func init() {
	syncCmd.Flags().StringVar(&glToken, "gltoken", "", "GitLab Access Token")
	syncCmd.Flags().StringVar(&adoToken, "adotoken", "", "Azure DevOps username:token")
	syncCmd.Flags().StringVar(&filename, "filename", "config.yaml", "Path to YAML file")
	syncCmd.MarkFlagRequired("gltoken")
	syncCmd.MarkFlagRequired("adotoken")
	rootCmd.AddCommand(syncCmd)
}

var syncCmd = &cobra.Command{
	Use:   "sync",
	Short: "Synchronize projects with Azure DevOps repos",
	Long:  `Ensures all the projects listed in the yaml config file have an associated pull mirror in the GitLab repo`,
	Run: func(cmd *cobra.Command, args []string) {
		results, err := parse.Parse(filename)
		if err != nil {
			log.Fatalf("Failed to parse: %v", filename)
			return
		}
		client, err := sync.NewClient(glToken)
		if err != nil {
			log.Fatalf("Failed to create GitLab client %v", err)
			return
		}
		sync.Sync(results, adoToken, client)
	},
}
