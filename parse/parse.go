package parse

import (
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

func Parse(filename string) (map[int]string, error) {
	// Open the YAML file
	file, err := os.Open(filename)
	if err != nil {
		log.Fatalf("Failed to open YAML file: %v", err)
		return nil, err
	}
	defer file.Close()

	// Parse the YAML file into a map
	var result map[int]string
	err = yaml.NewDecoder(file).Decode(&result)
	if err != nil {
		log.Fatalf("Failed to decode YAML: %v", err)
		return nil, err
	}

	return result, nil
}
